import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AppSettings } from './../../modules/shared/app.settings';
import { UserCommonService } from './../../modules/shared/services/user-common.service';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { UiService } from '../../modules/shared/services/ui.service';
import { StorageService } from './../../modules/shared/services/storage.service';
import { CommonBindingDataService } from '../../modules/shared/services/common-binding-data.service';
import { SidebarService } from '../../modules/shared/services/sidebar.service';
import { Message } from 'primeng/components/common/api';
import { SelectItem } from 'primeng/primeng';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html'
})
export class SignInComponent implements OnInit {

  country: SelectItem[] = [];
  language: SelectItem[] = [];
  msgs: Message[] = [];
  CountryLangData = [];
  langObj = [];

  signInForm = new FormGroup({
    cpf: new FormControl(),
    password: new FormControl(),
    country: new FormControl(),
    language: new FormControl()
  });

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private signInService: UserCommonService,
    private storageService: StorageService
  ) {
    this.getCountryLanguageList();
  }

  ngOnInit() {
    this.signInForm = this.formBuilder.group({
      cpf: ['', [Validators.required]],
      password: ['', [Validators.required]],
      country: ['', [Validators.required]],
      language: ['', [Validators.required]]
    });
  }

  getCountryLanguageList(){
    this.signInService.getCountryLanguageList().subscribe(results => {
      this.CountryLangData = results;
      this.loadCountries(results);
    }, (error) => {
      console.log(error);
    });
  }

  loadCountries(results) {
    results.filter((item) => {
      this.country.push({ label: item.Country.CounrtyName, value: item.Country.CountryID });
    });
  }

  loadCountryLang() {
    const countryId = this.signInForm.controls['country'].value;
    this.language = [];
    this.langObj = this.CountryLangData.find((item) => {
      if (item.Country.CountryID === countryId) {
        return item;
      }
    });
    console.log(this.langObj);
    this.langObj['LanguageList'].filter((item) => {
      this.language.push({ label: item.LanguageName, value: item.LanguageID });
    });
  }

  doSignIn() {
    const data = {
      CPF: this.signInForm.controls.cpf.value,
      Password: this.signInForm.controls.password.value
    };
    this.signInService.signIn(data).subscribe(results => {
      console.log(results);
      this.loadDataAfterLogin(results);
    }, (error) => {
      this.onAPIValidation(error);
    });
  }

  loadDataAfterLogin(results) {
    if (results !== undefined && results.Token !== undefined) {
      this.storageService.setItem(AppSettings.TOKEN_KEY, results.Token);
      this.storageService.setItem(AppSettings.USER_DETAILS, results);
      this.storageService.setItem(AppSettings.USER_COUNTRY, this.langObj);
      this.router.navigate(['/dashboard']);
    }
  }

  onAPIValidation(error) {
    if (error !== undefined && error.Message !== undefined) {
      this.msgs = [];
      this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.Message });
    }
  }
}
