import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AppSettings } from './../../modules/shared/app.settings';
import { AppConstant } from './../../modules/shared/app.constant.service';
import { UserCommonService } from './../../modules/shared/services/user-common.service';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Message } from 'primeng/components/common/api';

import { UiService } from '../../modules/shared/services/ui.service';
import { StorageService } from './../../modules/shared/services/storage.service';
import { CommonBindingDataService } from '../../modules/shared/services/common-binding-data.service';
import { SidebarService } from '../../modules/shared/services/sidebar.service';
import { isInvalidCpf } from '../../modules/shared/services/cpf.validator';
import { isInvalidDob } from '../../modules/shared/services/dob.validator';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html'
})
export class SignUpComponent implements OnInit {

  msgs: Message[] = [];

  signUpForm = new FormGroup({
    cpf: new FormControl(),
    name: new FormControl(),
    email: new FormControl(),
    phone: new FormControl(),
    dob: new FormControl(),
    password: new FormControl()
  });

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private signUpService: UserCommonService,
    private storageService: StorageService
  ) { }

  ngOnInit() {
    this.signUpForm = this.formBuilder.group({
      cpf: ['', [Validators.required, isInvalidCpf]],
      name: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.pattern(AppConstant.EMAIL_PATTERN)]],
      phone: ['', [Validators.required, Validators.pattern(AppConstant.DIGIT_PATTERN), Validators.maxLength(AppConstant.PHONE_MAX_LENGTH)]],
      dob: ['', [Validators.required, isInvalidDob]],
      password: ['', [Validators.required]],
    });
  }

  doSignUp() {
    const data = {
      Email: this.signUpForm.controls.email.value,
      Document: this.signUpForm.controls.cpf.value,
      Password: this.signUpForm.controls.password.value,
      Name: this.signUpForm.controls.name.value,
      PhoneNumber: this.signUpForm.controls.phone.value,
      DateOfBirth: this.signUpForm.controls.dob.value
    };
    this.signUpService.signUp(data).subscribe(results => {
      console.log(results);
      this.loadDataAfterLogin(results);
    }, (error) => {
      this.onAPIValidation(error);
    });
  }

  loadDataAfterLogin(results) {
    if (results !== undefined && results.Token !== undefined) {
      this.storageService.setItem(AppSettings.TOKEN_KEY, results.Token);
      this.storageService.setItem(AppSettings.USER_DETAILS, results);
      this.router.navigate(['/dashboard']);
    }
  }

  onAPIValidation(error) {
    console.log(error);
    if (error !== undefined && error.Message !== undefined) {
      this.msgs = [];
      this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.Message });
    }
  }

}
