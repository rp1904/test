import { HttpModule } from '@angular/http';
import * as moment from 'moment';
import { AppComponent } from './app.component';
import { AppHomeComponent } from './pages/app-home/app-home.component';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './modules/shared/shared.module';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { SignInComponent } from './pages/sign-in/sign-in.component';
import { Directive, ElementRef, Input, Renderer } from '@angular/core';
import { InputTextModule } from 'primeng/components/inputtext/inputtext';
import { StorageService } from './modules/shared/services/storage.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateService, TranslatePipe, TranslateDirective } from '@ngx-translate/core';
import { LoaderService } from './modules/shared/components/loader/loader.service';

import { EditorModule } from 'primeng/components/editor/editor';
import { PanelModule } from 'primeng/components/panel/panel';
import { GrowlModule } from 'primeng/components/growl/growl';
import { DialogModule } from 'primeng/components/dialog/dialog';
import { ButtonModule } from 'primeng/components/button/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmationService } from 'primeng/components/common/api';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { FieldsetModule } from 'primeng/components/fieldset/fieldset';
import { FileUploadModule } from 'primeng/components/fileupload/fileupload';
import { MultiSelectModule } from 'primeng/components/multiselect/multiselect';
import { OverlayPanelModule } from 'primeng/components/overlaypanel/overlaypanel';
import { ConfirmDialogModule } from 'primeng/components/confirmdialog/confirmdialog';
import { DropdownModule } from 'primeng/components/dropdown/dropdown';
import { SignUpComponent } from './pages/sign-up/sign-up.component';

const primengModules = [
  GrowlModule,
  PanelModule,
  ButtonModule,
  DialogModule,
  EditorModule,
  DropdownModule,
  CalendarModule,
  FieldsetModule,
  InputTextModule,
  FileUploadModule,
  MultiSelectModule,
  OverlayPanelModule,
  ConfirmDialogModule
];

@NgModule({
  declarations: [
    AppComponent,
    SignInComponent,
    AppHomeComponent,
    SignUpComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    primengModules,   // Prime ng modules
    AppRoutingModule, // App routing modules
    SharedModule.forRoot()
  ],
  providers: [StorageService, ConfirmationService, LoaderService],
  bootstrap: [AppComponent]
})
export class AppModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AppModule
    };
  }
  constructor() {
  }
}
