import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SignInComponent } from './pages/sign-in/sign-in.component';
import { SignUpComponent } from './pages/sign-up/sign-up.component';
import { AppHomeComponent } from './pages/app-home/app-home.component';
import { AuthenticationValidationService } from './modules/shared/services/authentication-validation.service';

export const routes: Routes = [

  { path: 'sign-in', component: SignInComponent },
  { path: 'sign-up', component: SignUpComponent },
  {
    path: '', component: AppHomeComponent,
    children: [
      {
        path: '',
        loadChildren: './modules/dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'create-booking',
        loadChildren: './modules/create-booking/create-booking.module#CreateBookingModule'
      },
      {
        path: 'buy-cads',
        loadChildren: './modules/buy-cads/buy-cads.module#BuyCadsModule'
      },
      {
        path: 'buy-saldo',
        loadChildren: './modules/buy-saldo/buy-saldo.module#BuySaldoModule'
      },
      {
        path: 'bonus-redemption',
        loadChildren: './modules/bonus-redemption/bonus-redemption.module#BonusRedemptionModule'
      },
      {
        path: 'registered-phones',
        loadChildren: './modules/registered-phones/registered-phones.module#RegisteredPhonesModule'
      },
      {
        path: 'registered-vehicles',
        loadChildren: './modules/registered-vehicles/registered-vehicles.module#RegisteredVehiclesModule'
      },
      {
        path: 'reports',
        loadChildren: './modules/reports/reports.module#ReportsModule'
      },
      {
        path: 'regularization',
        loadChildren: './modules/regularization/regularization.module#RegularizationModule'
      },
      {
        path: 'registration',
        loadChildren: './modules/registration/registration.module#RegistrationModule'
      },
      {
        path: 'change-password',
        loadChildren: './modules/change-password/change-password.module#ChangePasswordModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }
