import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

import { UserCommonService } from '../../../shared/services/user-common.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html'
})
export class ChangePasswordComponent implements OnInit {

  new_password;
  cnf_password;
  isValidate = false;
  message: any[];
  changePasswordFrom = new FormGroup({
    currentPassword: new FormControl(),
    newPassword: new FormControl(),
    cnfPassword: new FormControl()
  });

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserCommonService
  ) { }

  ngOnInit() {
    this.changePasswordFrom = this.formBuilder.group({
      currentPassword: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(10)]],
      newPassword: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(10)]],
      cnfPassword: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(10)]],
    });
  }

  changePaasswordAction(event: any) {
    this.message = [];
    const data = {
      UserId: 1,
      OldPassword: this.changePasswordFrom.get('currentPassword').value,
      NewPassword: this.changePasswordFrom.get('newPassword').value
      // cnfPassword: this.changePasswordFrom.get('cnfPassword').value
    };
    this.userService.passwordChange(data).subscribe(result => {
      this.message.push({ severity: 'success', summary: 'Success Message', detail: result.Message });
      this.changePasswordFrom.reset();
    }, (error) => {
      this.message.push({ severity: 'error', summary: 'Error Message', detail: error.Message });
    });
  }

  // Confirm password validation
  newPass(value: any) {
    this.new_password = value;
  }

  cnfPass(value: any) {
    this.cnf_password = value;
    if (this.cnf_password.length > 5 && this.cnf_password.length < 11) {
      if (this.new_password === this.cnf_password) {
        this.isValidate = false;
      } else {
        this.isValidate = true;
      }
    } else {
      this.isValidate = false;
    }
  }
}
