import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PasswordModule } from 'primeng/primeng';

import { SharedModule } from '../shared/shared.module';
import { ChangePasswordRoutingModule } from './change-password-routing.module';
import { ChangePasswordComponent } from './pages/change-password/change-password.component';

@NgModule({
  imports: [
    CommonModule,
    ChangePasswordRoutingModule,
    PasswordModule,
    SharedModule
  ],
  declarations: [ChangePasswordComponent]
})
export class ChangePasswordModule { }
