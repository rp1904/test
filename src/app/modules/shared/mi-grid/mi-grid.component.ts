import { Component, OnInit, OnDestroy, AfterViewInit, ElementRef, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { GridOptions } from 'ag-grid';
import { GridService } from './grid.service';
import { AppSettings } from './../app.settings';
import { UiService } from './../services/ui.service';

@Component({
  selector: 'app-mi-grid',
  providers: [GridService],
  template: ` <div style="width: 100%; height: 100%; margin : 0 auto;" (window:resize)="onResize($event)">
                <div class="mi-grid-action-header">
                      <div class="input-wrapper">
                           <input (keyup)="onQuickFilterChanged($event)" *ngIf="display"  type="text" id="quickFilterInput"
                      placeholder="Enter text to search..."
                      class="mi-grid-search form-field ui-inputtext ui-state-default" />
                          <label for="stuff" class="fa fa-search input-icon"></label>
                        </div>
                  
                  <span class="header-action-btn export" (click)="exportGrid($event)" title="Download XLS">
                      XLS <i class="fa fa-fw fa-file-excel-o" aria-hidden="true" ></i>
                  </span>
                  <span class="header-action-btn print" (click)="printGrid($event)" title="Download PDF">
                     PDF <i class="fa fa-fw fa-file-pdf-o" aria-hidden="true" ></i>
                  </span>
                </div>
              <ag-grid-angular   #agGrid  style="width: 100%; height: 100%;" class={{defaultTheme}} headerHeight="48" rowHeight="74"
               [gridOptions]="gridOptions"  (rowClicked)="onRowClicked($event)"> 
                </ag-grid-angular>
              <div>`
})
export class MiGridComponent implements OnInit, AfterViewInit, OnDestroy {
  subscription: Subscription;
  gridOptions: GridOptions;
  rowData: any[] = [];
  @Output() rowClicked = new EventEmitter<any>();
  @Output() getGridReference: EventEmitter<any> = new EventEmitter();
  @Input() url: any = '';
  @Input() printUrl: any = '';
  @Input() exportUrl: any = '';
  @Input() columnDefs: any[] = [];
  @Input() enableServerSideSorting: any = true;
  @Input() rowSelection: any = 'single';
  @Input() enableSorting: any = true;
  @Input() pagination: any = true;
  @Input() paginationPageSize: any = 10;
  @Input() paginationStartPage: any = 0;
  @Input() maxPagesInCache: any = 0;
  @Input() enableColResize: any = true;
  @Input() defaultTheme: any = 'ag-blue';
  @Input() animateRows: any = true;
  @Input() enableServerSideFilter: any = true;
  @Input() enableFilter: any = false;
  @Input() rowModelType: any = 'pagination';
  @Input() overlayLoadingTemplate: any = '<span class="ag-overlay-loading-center" "style=background:red">' +
  'Please wait while your rows are loading</span>';
  @Input() serialNo = false;

  gridUrlParams = '';
  listData: any[];
  display: any = false;

  dataSource = {
    getRows: (params: any) => {
      this.searchIncident(params.startRow, this.gridOptions.paginationPageSize, params, function(result, obj) {
        const rowCount = result.count;
        const rows = result.data;
        if (obj === true) {
          if (rows !== null && rows !== undefined) {
            rows.forEach(function(data, index) {
              data.rowid = (index + 1);
            });
          }
        }
        if (rowCount > 10) {
          document.querySelector('.gridparent').setAttribute('style', 'height:860px;');
        } else {
          // 120 header & serch
          // 77 grid row height
          document.querySelector('.gridparent').setAttribute('style', 'height:' + (120 + (77 * rowCount)) + 'px;');
          if (rowCount === 0) {
            document.querySelector('.gridparent').setAttribute('style', 'height:300px;');
          }
        }
        setTimeout(function() {
          params.successCallback(rows, rowCount);
        }, 500);
      });
    }
  };

  constructor(private elm: ElementRef, private gridService: GridService, private uiService: UiService, ) {
    this.gridOptions = <GridOptions>{
      context: {},
      defaultColDef: {
        suppressMenu: true,
      }
    };

  }
  ngOnInit() {

    this.gridOptions.columnDefs = this.columnDefs;
    this.gridOptions.rowSelection = this.rowSelection;
    this.gridOptions.enableSorting = this.enableSorting;
    this.gridOptions.enableFilter = this.enableFilter;
    this.gridOptions.paginationPageSize = this.paginationPageSize;
    this.gridOptions.paginationStartPage = this.paginationStartPage;
    this.gridOptions.enableColResize = this.enableColResize;
    this.gridOptions.animateRows = this.animateRows;
    this.gridOptions.enableServerSideSorting = this.enableServerSideSorting;
    this.gridOptions.overlayLoadingTemplate = this.overlayLoadingTemplate;
    this.gridOptions.enableServerSideFilter = this.enableServerSideFilter;
    this.gridOptions.maxPagesInCache = this.maxPagesInCache;
    this.gridOptions.rowModelType = this.rowModelType;
    this.gridOptions.suppressMenuHide = false;
    this.gridOptions.suppressAutoSize = true;
    this.gridOptions.unSortIcon = true;


    if (this.printUrl.length === 0) {
      this.elm.nativeElement.querySelector('.header-action-btn.print').setAttribute('style', 'display:none;');
    }
    if (this.exportUrl.length === 0) {
      this.elm.nativeElement.querySelector('.header-action-btn.export').setAttribute('style', 'display:none;');
    }

    this.subscription = this.uiService.sidebarToggledEvent
      .subscribe((state: string) => {
        this.refreshList();
      });
  }

  onResize(event: any) {
    this.refreshList();
  };

  refreshList() {
    this.gridOptions.api.doLayout();
    this.gridOptions.api.sizeColumnsToFit();
  }

  ngAfterViewInit() {
    const c = this;
    setTimeout(function() {
      if (c.gridOptions.api) {
        c.gridOptions.api.sizeColumnsToFit();
        if (c.url !== '') {
          c.gridOptions.api.setDatasource(c.dataSource);
        }

        c.getReferenceObject(c);
      }

      if (c.url !== '') {
        c.display = true;
        document.querySelector('[ref="btNext"]').setAttribute('class', 'ag-paging-button bt-next');
        document.querySelector('[ref="btLast"]').setAttribute('class', 'ag-paging-button bt-last');
        document.querySelector('[ref="btFirst"]').setAttribute('class', 'ag-paging-button bt-first');
        document.querySelector('[ref="btPrevious"]').setAttribute('class', 'ag-paging-button bt-previous');
        document.querySelector('[ref="lbCurrent"]').setAttribute('class', 'ag-paging-button lb-current');
        document.querySelector('[ref="lbFirstRowOnPage"]').setAttribute('class', 'ag-paging-button row-label');
        document.querySelector('[ref="lbLastRowOnPage"]').setAttribute('class', 'ag-paging-button row-label');
      } else {
        c.elm.nativeElement.querySelector('.ag-paging-panel').setAttribute('style', 'display:none;');
      }
    }, 100);
  }

  searchIncident(start: number, end: number, param: any, callback) {
    let searchText = '';
    let sortColumn = '';
    let sortType = '';
    let sortOptions = '';
    if (this.gridOptions.quickFilterText) {
      searchText = this.gridOptions.quickFilterText;
    }
    let params: string;
    if (this.url.lastIndexOf('?') < 0) {
      params = '?searchText=' + searchText + '&offset=' + start + '&limit=' + end;
      this.gridUrlParams = '';
    } else {
      params = '&searchText=' + searchText + '&offset=' + start + '&limit=' + end;
      this.gridUrlParams = this.url.substring(this.url.lastIndexOf('?'));
    }


    if (param.sortModel.length > 0) {
      sortColumn = param.sortModel[0].colId;
      sortType = param.sortModel[0].sort;
    }

    sortOptions = '&sortColumn=' + sortColumn + '&sortType=' + sortType;
    this.gridUrlParams += params + sortOptions;
    this.gridService.loadGridData(this.url + params + sortOptions).subscribe(
      (data: any) => callback(data, this.serialNo));
  }
  public onQuickFilterChanged($event) {
    if ($event.key === 'Enter') {
      this.gridOptions.quickFilterText = ($event.target.value).trim();
      this.gridOptions.api.setQuickFilter($event.target.value);
    }
  }

  public onRowClicked(e) {
    this.rowClicked.emit(e);
  }
  getReferenceObject(gridRef: any) {
    this.getGridReference.emit(gridRef);
  }

  private changeDatasource(url: any, columns: any) {
    this.url = url;
    this.columnDefs = columns;
    this.gridOptions.api.sizeColumnsToFit();
    this.gridOptions.api.setColumnDefs(columns);
    this.gridOptions.api.setDatasource(this.dataSource);
    this.gridOptions.api.refreshView();
    this.display = true;
    this.gridOptions.api.sizeColumnsToFit();
  }

  public exportGrid(event: any) {
  }

  public printGrid(event: any) {
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
