import { Injectable } from '@angular/core';
import { Response, Http } from '@angular/http';
import { RestApiService } from '../services/rest-api.service';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class GridService {

  constructor(private restApiService: RestApiService, private http: Http) { }

  public loadGridData(url: string): Observable<Response> {
    return this.restApiService.get(url, 'page-center');
    // console.log(url);
    // return this.http.get(url).map((Response) => Response.json());
  }

  public exportGrid(url: string) {
    // return this.restApiService.excel(url, 'export.xls', 'page-center');
  }

  public printGrid(url: string) {
    // return this.restApiService.pdf(url, 'print.pdf', 'page-center');
  }
}
