import { Router } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MenuItem } from 'primeng/primeng';
import { DomSanitizer } from '@angular/platform-browser';

import { StorageService } from './../../services/storage.service';
import { AppSettings } from './../../app.settings';
import { AuthenticationValidationService } from '../../services/authentication-validation.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {

  userMenuItems: MenuItem[];
  userLangItems: MenuItem[];
  userName: string;
  // profileImgObj: any = this.storageService.getItem(AppSettings.USER_IMG);
  // profileUrl = this.sanitize(this.profileImgObj);
  profileUrl: any = 'assets/images/profile.png';

  constructor(
    private router: Router,
    private storageService: StorageService,
    private sanitizer: DomSanitizer,
    private authenticationValidationService: AuthenticationValidationService
  ) { }

  ngOnInit() {
    const userLangObj: any = this.storageService.getItem(AppSettings.USER_COUNTRY);
    console.log(userLangObj);
    const userObj: any = this.storageService.getItem(AppSettings.USER_DETAILS);
    if (userObj !== undefined && userObj !== null) {
      this.userName = userObj.Name;
    }

    this.userMenuItems = [
      {
        label: 'Edit Profile', icon: 'fa fa-user-circle-o', command: (event) => {
          this.router.navigate(['/registration']);
        }
      },
      {
        label: 'Change password', icon: 'fa fa-key', command: (event) => {
          this.router.navigate(['/change-password']);
        }
      },
      {
        label: 'Logout', icon: 'fa fa-sign-out', command: (event) => {
          this.doSignOut();
        }
      }
    ];

    this.userLangItems = [
      {
        label: 'Edit Profile', icon: 'fa fa-user-circle-o', command: (event) => {
          this.router.navigate(['/registration']);
        }
      },
      {
        label: 'Change password', icon: 'fa fa-key', command: (event) => {
          this.router.navigate(['/change-password']);
        }
      },
      {
        label: 'Logout', icon: 'fa fa-sign-out', command: (event) => {
          this.doSignOut();
        }
      }
    ];
  }

  hamburgerClick($event) {
    const body = document.getElementsByTagName('body')[0];
    body.classList.toggle('closed');
  }

  doSignOut() {
    this.storageService.removeAll();
    this.authenticationValidationService.publish({
      isAuthenticate: false
    });
    this.router.navigate(['/sign-in']);
  }

  sanitize(url: string) {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }
}
