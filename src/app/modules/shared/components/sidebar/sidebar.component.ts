import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/components/common/api';

import { StorageService } from './../../services/storage.service';
import { SidebarService } from './../../services/sidebar.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html'
})

export class SidebarComponent implements OnInit {
  items: MenuItem[];

  constructor(private storageService: StorageService,
    private sidebarService: SidebarService, ) { }

  private manageActiveStatus() {

    // TODO: Following logic needs improvement for sub pages in the section
    // for expanded menu scenarios.

  }

  ngOnInit() {
    this.items = [
      {
        label: 'Dashboard',
        icon: 'dashboard-icon',
        routerLink: ['/dashboard']
      },
      {
        label: 'Create Booking',
        icon: 'booking-icon',
        routerLink: ['/create-booking']
      },
      {
        label: 'Buy Saldo',
        icon: 'saldo-icon',
        routerLink: ['/buy-saldo']
      },
      {
        label: 'Bonus Redemption',
        icon: 'bonus-icon',
        routerLink: ['/bonus-redemption']
      },
      {
        label: 'Regularization',
        icon: 'regularization-icon',
        routerLink: ['/regularization']
      },
      {
        label: 'Registration Data',
        icon: 'data-icon',
        routerLink: ['/registration']
      },
      {
        label: 'Change Password',
        icon: 'password-icon',
        routerLink: ['/change-password']
      },
      {
        label: 'Registered Phones',
        icon: 'phones-icon',
        routerLink: ['/registered-phones']
      },
      {
        label: 'Registered Vehicles',
        icon: 'vehicles-icon',
        routerLink: ['/registered-vehicles']
      },
      {
        label: 'Reports',
        icon: 'reports-icon',
        routerLink: ['/reports']
      }
    ];
  }
}
