import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgModule, ModuleWithProviders } from '@angular/core';
import {
  TranslateModule, TranslateLoader, TranslateService,
  TranslatePipe, TranslateDirective, MissingTranslationHandler
} from '@ngx-translate/core';

import { MenuModule } from 'primeng/components/menu/menu';
import { ChartModule } from 'primeng/components/chart/chart';
import { GrowlModule } from 'primeng/components/growl/growl';
import { ButtonModule } from 'primeng/components/button/button';
import { DialogModule } from 'primeng/components/dialog/dialog';
import { EditorModule } from 'primeng/components/editor/editor';
import { RatingModule } from 'primeng/components/rating/rating';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BlockUIModule } from 'primeng/components/blockui/blockui';
import { ConfirmationService } from 'primeng/components/common/api';
import { CheckboxModule } from 'primeng/components/checkbox/checkbox';
import { InputTextModule } from 'primeng/components/inputtext/inputtext';
import { DropdownModule } from 'primeng/components/dropdown/dropdown';
import { PanelMenuModule } from 'primeng/components/panelmenu/panelmenu';
import { FileUploadModule } from 'primeng/components/fileupload/fileupload';
import { MultiSelectModule } from 'primeng/components/multiselect/multiselect';
import { RadioButtonModule } from 'primeng/components/radiobutton/radiobutton';
import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { OverlayPanelModule } from 'primeng/components/overlaypanel/overlaypanel';
import { ConfirmDialogModule } from 'primeng/components/confirmdialog/confirmdialog';
import { TabViewModule } from 'primeng/components/tabview/tabview';
import { SpinnerModule } from 'primeng/components/spinner/spinner';
import { SelectButtonModule } from 'primeng/components/selectbutton/selectbutton';
import { CalendarModule } from 'primeng/components/calendar/calendar';

import { UiService } from './services/ui.service';
import { AppConstant } from './app.constant.service';
import { RestApiService } from './services/rest-api.service';
import { UserCommonService } from './services/user-common.service';
import { StorageService } from './services/storage.service';
import { ErrorBindingService } from './services/error-binding.service';
import { CommonBindingDataService } from './services/common-binding-data.service';
import { SidebarService } from './services/sidebar.service';
import { AuthenticationValidationService } from './services/authentication-validation.service';
import { HeaderComponent } from './components/header/header.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { MiTranslateLoaderService } from './services/mi-translate-loader.service';
import { MiMissingTranslationHandlerService } from './services/mi-missing-translation-handler.service';
import { AgGridModule } from 'ag-grid-angular/main';
import { MiGridComponent } from './mi-grid/mi-grid.component';
import { AgoPipe } from './pipes/ago.pipe';
import { DateFormatPipe } from './pipes/dateformat.pipe';
import { DateTimeFormatPipe } from './pipes/datetimeformat.pipe';
import { MinutsFormatPipe } from './pipes/minutesformat.pipe';
import { CapitalizePipe } from './pipes/capitalize.pipe';
import { UppercaseDirective } from './directives/capitalize.directive';
import { VehiclePlateDirective } from './directives/vehicle-plate.directive';
import { DigiPareCommonService } from './services/digipare-common.service';
import { LoaderComponent } from './components/loader/loader.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    PanelMenuModule,
    RatingModule,
    ButtonModule,
    EditorModule,
    ConfirmDialogModule,
    GrowlModule,
    ChartModule,
    MenuModule,
    DropdownModule,
    RadioButtonModule,
    CheckboxModule,
    DialogModule,
    FormsModule,
    FileUploadModule,
    ReactiveFormsModule,
    AutoCompleteModule,
    BlockUIModule,
    OverlayPanelModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useClass: MiTranslateLoaderService
      },
      missingTranslationHandler: {
        provide: MissingTranslationHandler,
        useClass: MiMissingTranslationHandlerService
      }
    }),
    AgGridModule.withComponents([])
  ],
  declarations: [
    HeaderComponent,
    SidebarComponent,
    MiGridComponent,
    AgoPipe,
    DateFormatPipe,
    DateTimeFormatPipe,
    MinutsFormatPipe,
    UppercaseDirective,
    VehiclePlateDirective,
    CapitalizePipe,
    LoaderComponent
  ],
  exports: [
    HeaderComponent,
    SidebarComponent,
    TranslateModule,
    ConfirmDialogModule,
    MiGridComponent,
    AgoPipe,
    DateFormatPipe,
    DateTimeFormatPipe,
    MinutsFormatPipe,
    FormsModule,
    ReactiveFormsModule,
    InputTextModule,
    TabViewModule,
    DropdownModule,
    RadioButtonModule,
    ButtonModule,
    SpinnerModule,
    AutoCompleteModule,
    SelectButtonModule,
    CalendarModule,
    CheckboxModule,
    DialogModule,
    GrowlModule,
    FileUploadModule,
    UppercaseDirective,
    VehiclePlateDirective,
    CapitalizePipe,
    LoaderComponent
  ]
})
export class SharedModule {

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [
        RestApiService,
        UiService,
        UserCommonService,
        StorageService,
        ErrorBindingService,
        CommonBindingDataService,
        AuthenticationValidationService,
        ConfirmationService,
        TranslateService,
        SidebarService,
        AppConstant,
        DigiPareCommonService
      ]
    };
  }

  constructor(translate: TranslateService) {
    // this language will be used as a fallback when a translation isn't found in the current language
    translate.setDefaultLang('en');

    // the lang to use, if the lang isn't available, it will use the current loader to get them
    translate.use('en');

  }

}
