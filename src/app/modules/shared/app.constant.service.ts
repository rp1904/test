import { Injectable } from '@angular/core';

@Injectable()
export class AppConstant {
  public static EMAIL_PATTERN = new RegExp(['^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$'].join(''));
  public static DIGIT_PATTERN = new RegExp(['^[\\d]*$'].join(''));
  public static PHONE_MAX_LENGTH = 11;

  // VEHICLE TYPE
  public VEHICLE_TYPE = { CAR: 0, MOTORCYCLE: 1, TRUCK: 3, BUS: 4 };
}

// Regular expression that accept 3 char and followed by space and 4 number.
export const vehiclePlatePattern = /^[a-zA-Z]{3}[_ ][0-9].{3}$/;
