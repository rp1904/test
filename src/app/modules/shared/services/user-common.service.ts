import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { RestApiService } from '../services/rest-api.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class UserCommonService {

  constructor(private restApiService: RestApiService) { }

  signOut(): Observable<any> {
    return this.restApiService.delete('/secure/signout', 'page-center');
  }

  signIn(data): Observable<any> {
    return this.restApiService.post('user/login', data, 'page-center');
  }

  signUp(data): Observable<any> {
    return this.restApiService.post('user/register', data, 'page-center');
  }

  passwordChange(data): Observable<any> {
    return this.restApiService.post('user/change-password', data, 'page-center');
  }

  forgotPassword(data): Observable<any> {
    return this.restApiService.post('/users/forgot-password', data, 'page-center');
  }

  getCountryLanguageList(): Observable<any> {
    return this.restApiService.get('/user/country-language', 'page-center');
  }

  getCtiyList(): Observable<any> {
    return this.restApiService.get('city', 'page-center');
  }

  getParkingCharge(data): Observable<any> {
    return this.restApiService.post('city/get-parking-charge', data, 'page-center');
  }

  getVehicleList(): Observable<any> {
    return this.restApiService.get('vehicle', 'page-center');
  }

  getPreBookingData(data): Observable<any> {
    return this.restApiService.post('parking/pre-booking-saopaulo', data, 'page-center');
  }

  saoPauloCreateBooking(data): Observable<any> {
    return this.restApiService.post('parking/book-parking-saopaulo', data, 'page-center');
  }

  createBookParking(data): Observable<any> {
    return this.restApiService.post('parking/book-parking', data, 'page-center');
  }

  searchHistory(data): Observable<any> {
    return this.restApiService.post('user/search-history', data, 'page-center');
  }

  addBonusRedemption(data): Observable<any> {
    return this.restApiService.post('regularization/redeem-bonus', data, 'page-center');
  }

}

