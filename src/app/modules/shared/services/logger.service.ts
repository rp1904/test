import { environment } from '../../../../environments/environment';

export class LoggerService {
    public static Debug(text: String) {
        if (!environment.production && text !== null && text !== '') {
            this.traceLog('Debug :::: ' + JSON.stringify(text));
        }
    }

    private static traceLog(text: String) {
        console.log(text);
    }
}
