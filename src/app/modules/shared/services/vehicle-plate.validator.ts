import { FormControl } from '@angular/forms';

import { vehiclePlatePattern } from '../app.constant.service';

interface FieldError {
  invalid?: boolean;
}

export function vehiclePlateValidator(formControl: FormControl): FieldError {
  const vehiclePlate = formControl.value;
  if (vehiclePlate && vehiclePlate !== '' && !vehiclePlatePattern.test(vehiclePlate)) {
    return {
      invalid: true
    };
  }
  return null;
}
