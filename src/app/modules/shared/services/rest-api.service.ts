import { Injectable, NgZone, EventEmitter } from '@angular/core';
import { RequestOptionsArgs, Response, Headers, Http, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AppSettings } from '../app.settings';
import { StorageService } from './storage.service';
import { Router } from '@angular/router';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { LoaderService } from './../components/loader/loader.service';

@Injectable()
export class RestApiService {

  constructor(private http: Http, private zone: NgZone, private router: Router,
    private storageService: StorageService, private loaderService: LoaderService) {
  }

  private prependApiUrl(url: string): string {
    return AppSettings.BASE_URL + '/' + url;
  }

  get(url: string, loader?: string, options: RequestOptionsArgs = { headers: this.getHeaders() }): Observable<Response> {
    this.showLoader(loader);
    return this.http.get(this.prependApiUrl(url), options).map((Response) => this.handleSuccess(Response))
      .catch((error) => this.handleError(error));
  }

  post(url: string, body: any, loader?: string, options: RequestOptionsArgs = { headers: this.getHeaders() }): Observable<Response> {
    this.showLoader(loader);
    return this.http.post(this.prependApiUrl(url), body, options).map((Response) => this.handleSuccess(Response))
      .catch((error) => this.handleError(error));
  }

  put(url: string, body?: any, loader?: string, options: RequestOptionsArgs = { headers: this.getHeaders() }): Observable<Response> {
    this.showLoader(loader);
    return this.http.put(this.prependApiUrl(url), body, options).map((Response) => this.handleSuccess(Response))
      .catch((error) => this.handleError(error));
  }

  delete(url: string, loader?: string, options: RequestOptionsArgs = { headers: this.getHeaders() }): Observable<Response> {
    this.showLoader(loader);
    return this.http.delete(this.prependApiUrl(url), options).map((Response) => this.handleSuccess(Response))
      .catch((error) => this.handleError(error));
  }

  patch(url: string, body: any, loader?: string, options: RequestOptionsArgs = { headers: this.getHeaders() }): Observable<Response> {
    this.showLoader(loader);
    return this.http.patch(this.prependApiUrl(url), body, options);
  }

  head(url: string, loader?: string, options: RequestOptionsArgs = { headers: this.getHeaders() }): Observable<Response> {
    this.showLoader(loader);
    return this.http.head(this.prependApiUrl(url), options);
  }

  options(url: string, loader?: string, options: RequestOptionsArgs = { headers: this.getHeaders() }): Observable<Response> {
    this.showLoader(loader);
    return this.http.options(this.prependApiUrl(url), options);
  }

  excel(url: string, fileName: string, loader?: string,
    options: RequestOptionsArgs = {
      headers: this.getHeaders(),
      responseType: ResponseContentType.Blob
    }) {
    this.showLoader(loader);
    return this.http.get(url, options).subscribe(data =>
      this.downloadFile(data, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', fileName)),
      error => this.handleError(error);
  }

  pdf(url: string, fileName: string, loader?: string,
    options: RequestOptionsArgs = {
      headers: this.getHeaders(),
      responseType: ResponseContentType.Blob
    }) {
    this.showLoader(loader);
    return this.http.get(url, options).subscribe(data => this.downloadFile(data, 'application/pdf', fileName)),
      error => this.handleError(error);
  }

  image(url: string, fileName: string, loader?: string, options: RequestOptionsArgs =
    { headers: this.getHeaders(), responseType: ResponseContentType.Blob }) {
    this.showLoader(loader);
    return this.http.get(url, options).subscribe(data => this.downloadFile(data, this.getContentType(fileName), fileName)),
      error => this.handleError(error);
  }

  private getHeaders(): Headers {
    const headers = new Headers();
    headers.append('Accept-Language', AppSettings.HEADER_ACCEPT_LANGUAGE);
    headers.append('Content-Type', AppSettings.HEADER_CONTENT_TYPE);
    headers.append('Accept', AppSettings.HEADER_CONTENT_TYPE);
    if (AppSettings.HEADER_AUTHORIZATION_VALUE !== null) {
      // headers.append('Authorization', '' + 'caf757bc-e349-4072-a962-5d48a8b06d04');
      headers.append('Authorization', '' + this.storageService.getItem(AppSettings.TOKEN_KEY));
    }
    return headers;
  }

  private handleError(error: Response | any) {
    this.hideLoader();
    const body = error.json() || '';
    if (error.status === 400) {
      if (error instanceof Response) {
        const err = body;
        return Observable.throw(err);
      }
    } else if (error.status === 500 || error.status === 403) {
      // const errMsg=body.general;
      return Observable.throw(body);
    } else if (error.status === 401) {
      console.log(error.status);
      this.router.navigate(['/sign-in']);
    } else if (error.status === 404) {
      return Observable.throw(body);
    } else if (error.status === 409) {
      return Observable.throw(body);
    } else if (error.status === 409) {
      const err = body;
      return Observable.throw(err);
    }
    // TODO handle 401 and other errors;
  }

  private handleSuccess(res: Response) {
    this.hideLoader();
    const body = res.json();
    return body || {};
  }

  downloadFile(data: Response, contentType: string, fileName: string) {
    const blob = new Blob([data.blob()], { type: contentType });
    const link = document.createElement('a');
    link.href = window.URL.createObjectURL(blob);
    link.download = fileName;
    link.click();
    this.hideLoader();
    setTimeout(link.remove(), 1000);
  }

  private getContentType(fileName: string) {
    const extension = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();
    switch (extension) {
      case 'jpeg':
        return 'image/jpeg';
      case 'jpg':
        return 'image/jpeg';
      case 'png':
        return 'image/png';
      case 'gif':
        return 'image/gif';
      case 'bmp':
        return 'image/x-ms-bmp';
      case 'pdf':
        return 'application/pdf';
      case 'xls':
        return 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
    }
    return '';
  }

  private onEnd(): void {
    this.hideLoader();
  }

  private showLoader(loader?: string): void {
    if (loader !== undefined && loader !== null && 'none' !== loader.toLowerCase()) {
      this.loaderService.show(loader);
    }
  }

  private hideLoader(): void {
    this.loaderService.hide();
  }
}
