import { TestBed, inject } from '@angular/core/testing';

import { MiTranslateLoaderService } from './mi-translate-loader.service';

describe('MiTranslateLoaderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MiTranslateLoaderService]
    });
  });

  it('should ...', inject([MiTranslateLoaderService], (service: MiTranslateLoaderService) => {
    expect(service).toBeTruthy();
  }));
});
