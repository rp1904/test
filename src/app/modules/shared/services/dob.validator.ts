import { AbstractControl } from '@angular/forms';
import * as moment from 'moment';

export function isInvalidDob(dobControl) {
    const dobYear = moment(dobControl.value).format('YYYY-M-DD');
    const years = moment().diff(dobYear, 'years');
    if (years < 18) {
        return { invalid: true };
    }
    return false;
}
