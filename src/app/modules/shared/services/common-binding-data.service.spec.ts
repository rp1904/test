import { TestBed, inject } from '@angular/core/testing';

import { CommonBindingDataService } from './common-binding-data.service';

describe('CommonBindingDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CommonBindingDataService]
    });
  });

  it('should ...', inject([CommonBindingDataService], (service: CommonBindingDataService) => {
    expect(service).toBeTruthy();
  }));
});
