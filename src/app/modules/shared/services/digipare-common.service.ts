import { Injectable } from '@angular/core';

@Injectable()
export class DigiPareCommonService {
    removeSpaces(value: String): String {
        return (value !== null && value !== '') ? value.replace(/ /g, '') : value;
    }
}
