import { TestBed, inject } from '@angular/core/testing';

import { ErrorBindingService } from './error-binding.service';

describe('ErrorBindingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ErrorBindingService]
    });
  });

  it('should ...', inject([ErrorBindingService], (service: ErrorBindingService) => {
    expect(service).toBeTruthy();
  }));
});
