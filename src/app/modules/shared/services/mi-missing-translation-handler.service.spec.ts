import { TestBed, inject } from '@angular/core/testing';

import { MiMissingTranslationHandlerService } from './mi-missing-translation-handler.service';

describe('MIMissingTranslationHandlerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MiMissingTranslationHandlerService]
    });
  });

  it('should ...', inject([MiMissingTranslationHandlerService], (service: MiMissingTranslationHandlerService) => {
    expect(service).toBeTruthy();
  }));
});
