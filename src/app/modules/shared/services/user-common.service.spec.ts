import { TestBed, inject } from '@angular/core/testing';

import { UserCommonService } from './user-common.service';

describe('UserCommonService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserCommonService]
    });
  });

  it('should ...', inject([UserCommonService], (service: UserCommonService) => {
    expect(service).toBeTruthy();
  }));
});
