import { Injectable, Output, EventEmitter } from '@angular/core';
import { AppSettings } from './../app.settings';
import { StorageService } from './../services/storage.service';
import { CommonBindingDataService } from './../services/common-binding-data.service';
import { Subject } from 'rxjs/Subject';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class SidebarService {
  private sideSubject = new Subject<any>();
  sideState = this.sideSubject.asObservable();
  dispatcherSideBar = [];
  adminSideBar = [];
  corporateSideBar = [];
  CurrentUsersCorporateId;

  constructor(private translateService: TranslateService,
    private commonService: CommonBindingDataService,
    private storageService: StorageService) {

  }

}
