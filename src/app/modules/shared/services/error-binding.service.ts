import { Injectable } from '@angular/core';
import { AppSettings } from '../app.settings';

@Injectable()
export class ErrorBindingService {

  constructor() { }

  onAPIValidationError(error, errorList) {
    error = JSON.parse(error);
    for (const k in error) {
      if (error.hasOwnProperty(k)) {
        errorList[0][k] = true;
        errorList[k] = error[k];
      }
    }
  }

  onAPIValidationSuccess(response, errorList) {
    for (const k in response) {
      if (response.hasOwnProperty(k)) {
        errorList[0][k] = true;
        errorList[k] = response[k];
      }
    }
  }

}
