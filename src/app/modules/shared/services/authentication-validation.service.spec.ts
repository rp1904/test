import { TestBed, inject } from '@angular/core/testing';

import { AuthenticationValidationService } from './authentication-validation.service';

describe('AuthenticationValidationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthenticationValidationService]
    });
  });

  it('should ...', inject([AuthenticationValidationService], (service: AuthenticationValidationService) => {
    expect(service).toBeTruthy();
  }));
});
