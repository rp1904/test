import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { RestApiService } from '../../shared/services/rest-api.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { FieldsetModule } from 'primeng/components/fieldset/fieldset';
import { StorageService } from './../../shared/services/storage.service';
import { AppSettings } from './../../shared/app.settings';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class CommonBindingDataService {

  constructor(private restApiService: RestApiService,
    private storageService: StorageService,
    private translateService: TranslateService
  ) { }

  getLabel(string) {
    let select;
    this.translateService.get(string).subscribe(translatedValue => {
      select = translatedValue;
    });
    return select;
  }

}
