import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { StorageService } from './storage.service';
import { AppSettings } from './../app.settings';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class AuthenticationValidationService implements CanActivate {

  private _subject = new Subject<any>();
  public event = this._subject.asObservable();

  constructor(private storageService: StorageService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.storageService.getItem(AppSettings.TOKEN_KEY)) {
      return true;
    }
    this.router.navigate(['/signin']);
    return false;
  }

  public publish(flag: object) {
    this._subject.next(flag);
  }
}
