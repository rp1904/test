import { Directive, Input, ElementRef, OnChanges, HostListener } from '@angular/core';

@Directive({
  selector: '[appUpperCase]'
})

export class UppercaseDirective implements OnChanges {
  @Input() appUpperCase: boolean;

  @HostListener('input')
  toUpperCase(value: any) {
    // Set uppercase value
    if (this.appUpperCase && this.ref.nativeElement.value != null) {
      this.ref.nativeElement.value = this.ref.nativeElement.value.charAt(0).toUpperCase() + this.ref.nativeElement.value.slice(1);
    }
  }

  constructor(private ref: ElementRef) {
  }

  ngOnChanges(changes) {
    this.toUpperCase(this.ref.nativeElement.value);
  }
}
