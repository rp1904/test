import { Directive, Input, ElementRef, OnChanges, HostListener } from '@angular/core';

@Directive({
  selector: '[appVehiclePlate]'
})

export class VehiclePlateDirective implements OnChanges {

  @Input() appVehiclePlate: boolean;

  @HostListener('input')
  addSpaceWordNumber(value: any) {
    if (this.ref.nativeElement.value !== null && this.ref.nativeElement.value !== '') {
      if (this.ref.nativeElement.value.length > 3) {
        const trimText = this.ref.nativeElement.value.replace(/ /g, '');
        this.ref.nativeElement.value = trimText.substr(0, 3) + ' ' + trimText.substr(3);
      }
      this.ref.nativeElement.value = this.ref.nativeElement.value.toUpperCase();
    }
  }

  constructor(private ref: ElementRef) {
  }

  ngOnChanges(changes) {
    this.addSpaceWordNumber(this.ref.nativeElement.value);
  }
}
