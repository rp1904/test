import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

@Injectable()
export class AppSettings {
  // public static BASE_URL = environment.basePath;
  public static BASE_URL = 'http://rest.areatecnologia.com.br/DigiPareAPI/api';
  public static HEADER_CONTENT_TYPE = 'application/json';
  public static HEADER_AUTHORIZATION = 'Authorization';
  public static HEADER_ACCEPT_LANGUAGE = 'en-US';
  public static HEADER_AUTHORIZATION_VALUE = '';
  public static USER: any = null;
  public static TOKEN_KEY = 'Token';
  public static USER_DETAILS = 'userDetails';
  public static USER_IMG = 'userImg';
  public static USER_COUNTRY = 'userCountry';

  // public static WEBSITE_PATTERN = new RegExp(['^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?',
  // '[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})',
  // '?(\/.*)?$'].join(''));
  // public static NAME_PATTERN = new RegExp(['^[A-Za-z\\d\-_\\s]*$'].join(''));
  // public static ROLE_PATTERN = new RegExp(['^[A-Za-z\\d\-_\\s/\\\\\]*$'].join(''));
  // public static PHONE_PATTERN = new RegExp(['^[0-9]'].join(''));
  // public static PHONE_PATTERN: any = /^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})?$/i;
  // public static FEE_PATTERN = new RegExp(['^\\d+(\.\\d{1,2})?$'].join(''));
  // public static ALPHA_NUMERIC = new RegExp(['^[A-Za-z0-9]'].join(''));
  // public static EMAIL_PATTERN = new RegExp(['^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$'].join(''));
  // public static ZIPCODE_PATTERN = new RegExp(['^\\d{6}$'].join(''));
  // public static PHONE_MAX_LENGTH = 11;

  public static DIGIT_PATTERN = new RegExp(['^[\\d]*$'].join(''));
  public static DIGIT_PATTERN_CONDITION = new RegExp(['^[1-9][0-9]{0,3}$'].join(''));
}
