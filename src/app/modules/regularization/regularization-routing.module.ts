import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegularizationComponent } from './pages/regularization/regularization.component';
import { RegularizationListComponent } from './pages/regularization-list/regularization-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'list' },
  { path: 'list', component: RegularizationListComponent, pathMatch: 'full' },
  { path: 'add-regularization', component: RegularizationComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegularizationRoutingModule { }
