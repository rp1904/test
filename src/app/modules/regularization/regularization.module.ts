import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { DropdownModule, ButtonModule } from 'primeng/primeng';
import { SharedModule } from '../../modules/shared/shared.module';

import { RegularizationRoutingModule } from './regularization-routing.module';
import { RegularizationComponent } from './pages/regularization/regularization.component';
import { RegularizationListComponent } from './pages/regularization-list/regularization-list.component';

@NgModule({
  imports: [
    CommonModule,
    RegularizationRoutingModule,
    ReactiveFormsModule,
    SharedModule,
    ButtonModule,
    DropdownModule
  ],
  declarations: [RegularizationComponent, RegularizationListComponent]
})
export class RegularizationModule { }
