import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegularizationListComponent } from './regularization-list.component';

describe('RegularizationListComponent', () => {
  let component: RegularizationListComponent;
  let fixture: ComponentFixture<RegularizationListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RegularizationListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegularizationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
