import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { SelectItem } from 'primeng/primeng';

import { UserCommonService } from '../../../shared/services/user-common.service';

@Component({
  selector: 'app-regularization-list',
  templateUrl: './regularization-list.component.html'
})
export class RegularizationListComponent implements OnInit {

  bonusData: any[];
  dataQuantity = 0;
  message: any[];
  selectDays: SelectItem[];
  selectedDays = 30;

  constructor(
    private router: Router,
    private uiService: UserCommonService
  ) {
    this.selectDays = [
      { label: '30 Days', value: 30 },
      { label: '60 Days', value: 60 },
      { label: '120 Days', value: 120 },
      { label: '180 Days', value: 180 }
    ];


  }

  ngOnInit() {
    this.getSearchedHistory();
  }

  getSelectedDays(event: any) {
    this.selectedDays = event.value;
    this.getSearchedHistory();
  }

  getSearchedHistory() {
    this.message = [];

    const data = {
      Country: null,
      Language: null,
      NumberOfDays: this.selectedDays,
      LicensePlates: [],
      Cities: [],
      BookingType: null,
      NumberOfRecord: 1000,
      PageNumber: 1,
      UserId: null,
      UserProfileId: null
    };

    this.uiService.searchHistory(data).subscribe(result => {
      this.bonusData = result.filter((value) => value.OperationType === 'REG');
      this.dataQuantity = this.bonusData.length;
    }, (error) => {
      console.log(error);
      this.message.push({ severity: 'error', summary: 'Error Message', detail: error.Message });
    });
  }

  navigateToAddReg(event: any) {
    this.router.navigate(['regularization/add-regularization'], {});
  }
}
