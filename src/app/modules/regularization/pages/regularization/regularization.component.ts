import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { SelectItem } from 'primeng/primeng';

@Component({
  selector: 'app-regularization',
  templateUrl: './regularization.component.html'
})
export class RegularizationComponent implements OnInit {
  cities: SelectItem[];

  regularizationForm = new FormGroup({
    selectedCity: new FormControl(),
    warningNumber: new FormControl(),
    numberPlate: new FormControl()
  });

  constructor(private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.cities = [];
    this.cities.push({ label: 'City', value: null });
    this.cities.push({ label: 'New York', value: { id: 1, name: 'New York', code: 'NY' } });
    this.cities.push({ label: 'Rome', value: { id: 2, name: 'Rome', code: 'RM' } });

    this.regularizationForm = this.formBuilder.group({
      selectedCity: ['', [Validators.required]],
      warningNumber: ['', [Validators.required]],
      numberPlate: ['', Validators.required]
    });
  }

  regularizationFormAction(value: any) {
    const data = {
      selectedCity: this.regularizationForm.get('selectedCity').value,
      warningNumber: this.regularizationForm.get('warningNumber').value,
      numberPlate: this.regularizationForm.get('numberPlate').value
    };
    console.log(data);
  }

  navigateToRegList(event: any) {
    this.router.navigate(['regularization/list'], {});
  }
}
