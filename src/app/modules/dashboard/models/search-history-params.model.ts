export class SearchHistoryParamsModel {
  'Country': String = '';
  'Language': String = '';
  'NumberOfDays': Number = 15;
  'LicensePlates': Array<string> = [''];
  'Cities': Array<string> = [''];
  'BookingType': String = '';
  'NumberOfRecord': Number = 10;
  'PageNumber': Number = 1;
  'UserId': Number = 0;
  'UserProfileId': Number = 0;
}
