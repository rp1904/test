export class DashboardParamsModel {
  'UserId': Number = 0;
  'UserProfileId': Number = 0;
  'Country': String;
  'Language': String;
}
