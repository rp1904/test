export class CurrentActiveParkingModel {
  'AreaID': string;
  'AreaName': string;
  'AreaStartTime': string;
  'AreaEndTime': string;
  'AuthorizationNumber': number;
  'BookingType': string;
  'BookingStartTime': string;
  'BookingEndTime': string;
  'BookingDuration': number;
  'BardCodeImgUrl': string;
  'CityID': string;
  'CityName': string;
  'ParkedLocation': string;
  'Latitude': string;
  'Longitude': string;
  'RuleNumber': number;
  'TransactionId': number;
  'VehicleName': string;
  'VehicleType': number;
  'VehicleLicensePlate': string;
}
