import { AccountBalanceModel } from './account-balance.model';
import { CurrentActiveParkingModel } from './current-active-parking.model';
import { VehicleModel } from './../../registered-vehicles/models/vehicle.model';

export class DashboardModel {
  AccountBalanceModel: AccountBalanceModel = new AccountBalanceModel();
  ActiveParkingCount: number;
  UsersCurrentActiveParkings: Array<CurrentActiveParkingModel>;
  UsersVehicle: Array<VehicleModel>;
  VehicleCount: number;
  constructor() {
  }
}
