import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InputTextModule, DropdownModule, CalendarModule, ButtonModule, AccordionModule, MultiSelectModule } from 'primeng/primeng';

import { SharedModule } from '../shared/shared.module';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { DashboardService } from './services/dashboard.service';

@NgModule({
  imports: [
    CommonModule,
    InputTextModule,
    DropdownModule,
    CalendarModule,
    ButtonModule,
    AccordionModule,
    MultiSelectModule,
    DashboardRoutingModule,
    SharedModule
  ],
  declarations: [DashboardComponent],
  providers: [DashboardService]
})
export class DashboardModule { }
