import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { RestApiService } from '../../shared/services/rest-api.service';

@Injectable()
export class DashboardService {

  constructor(private restApiService: RestApiService) { }

  dashboard(params): Observable<any> {
    return this.restApiService.post('user/home', params, 'page-center');
  }

  searchHistory(params): Observable<any> {
    return this.restApiService.post('user/search-history', params, 'page-center');
  }
}
