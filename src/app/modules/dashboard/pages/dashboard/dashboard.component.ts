import { Component, OnInit, OnDestroy, ElementRef, ViewChild, Renderer2 } from '@angular/core';
import { SelectItem } from 'primeng/primeng';

import { DashboardService } from './../../services/dashboard.service';
import { UserCommonService } from './../../../shared/services/user-common.service';
import { CommonBindingDataService } from './../../../shared/services/common-binding-data.service';
import { DashboardParamsModel } from './../../models/dashboard-params.model';
import { DashboardModel } from './../../models/dashboard.model';
import { SearchHistoryParamsModel } from './../../models/search-history-params.model';
import { VehicleModel } from './../../../registered-vehicles/models/vehicle.model';
import { LoggerService } from '../../../shared/services/logger.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {
  bookings: Array<any>;
  totalBookings;
  dashboardParams: DashboardParamsModel = new DashboardParamsModel();
  dashboardData: DashboardModel = new DashboardModel();
  searchHistoryParams: SearchHistoryParamsModel = new SearchHistoryParamsModel();
  searchHistoryData;
  cities: SelectItem[];
  selectedCity;
  vehicles: SelectItem[];
  selectedVehicle;

  scrollWidth;
  carouselLength;
  carouselWidth;
  carouselViewPort;
  tsCoord;
  diff = 0;
  rightPos = 0;
  isListenerActive = false;
  mouseMoveListener: Function;
  mouseUpListener: Function;
  @ViewChild('cardWrapper') cardElm: ElementRef;
  @ViewChild('carouselScroll') scrollElm: ElementRef;
  constructor(
    private renderer: Renderer2,
    private dashboardService: DashboardService,
    private userService: UserCommonService,
    private commonService: CommonBindingDataService
  ) {
    this.bookings = [
      { vin: 'r3278r2', year: 2010, brand: 'Audi', color: 'Black' },
      { vin: 'jhto2g2', year: 2015, brand: 'BMW', color: 'White' },
      { vin: 'h453w54', year: 2012, brand: 'Maruti', color: 'Blue' },
      { vin: 'g43gwwg', year: 1998, brand: 'Renault', color: 'White' },
      { vin: 'gf45wg5', year: 2011, brand: 'VW', color: 'Red' },
      { vin: 'bhv5y5w', year: 2015, brand: 'Jaguar', color: 'Blue' },
      { vin: 'ybw5fsd', year: 2012, brand: 'Ford', color: 'Yellow' },
      { vin: '45665e5', year: 2011, brand: 'Mercedes', color: 'Brown' },
      { vin: 'he6sb5v', year: 2015, brand: 'Ford', color: 'Black' }
    ];
  }


  ngOnInit() {
    this.getDashboardData();
    this.getBookingHistory();
    this.getVehicleList();
    this.getCityList();
  }

  getDashboardData() {
    this.dashboardService.dashboard(this.dashboardParams).subscribe((result) => {
      this.dashboardData = result;
      this.calculateCarouselScrollLimit();
    }, (err) => {
      console.log(err);
    });
  }

  getBookingHistory() {
    this.dashboardService.searchHistory(this.searchHistoryParams).subscribe((result) => {
      LoggerService.Debug(result);
      this.searchHistoryData = result;
    }, (err) => {
      console.log(err);
    });
  }

  getVehicleList() {
    this.userService.getVehicleList().subscribe((result) => {
      this.vehicles = this.setMultiselectData(result, 'VehicleName', 'Plate');
    }, (err) => {
      console.log(err);
    });
  }

  getCityList() {
    this.userService.getCtiyList().subscribe((result) => {
      this.cities = this.setMultiselectData(result, 'CityName', 'CityId');
    }, (err) => {
      console.log(err);
    });
  }

  setMultiselectData(data, labelProp, valueProp) {
    const resultData = [];
    data.forEach((element, idx) => {
      resultData[idx] = {
        label: element[labelProp],
        value: element[valueProp]
      };
    });
    return resultData;
  }

  toggleDetailView(idx) {
    this.searchHistoryData[idx].detailView = !this.searchHistoryData[idx].detailView;
  }

  calculateCarouselScrollLimit() {
    this.totalBookings = this.dashboardData.UsersCurrentActiveParkings.length;
    this.carouselLength = (370 * this.totalBookings);
    this.carouselViewPort = this.cardElm.nativeElement.offsetWidth < this.carouselLength ?
      (this.cardElm.nativeElement.offsetWidth + 10) : this.carouselLength;
    this.carouselWidth = this.carouselLength - (this.carouselViewPort);

    this.scrollWidth = ((this.carouselViewPort) / (370 * this.totalBookings)) * 100;

    this.renderer.setStyle(this.scrollElm.nativeElement, 'width', this.scrollWidth + '%');
  }

  scrollTheCarousel(coord) {
    this.diff = this.rightPos + (this.tsCoord - coord);
    if (this.diff < 0) {
      this.diff = 0;
    }
    if (this.diff > this.carouselWidth) {
      this.diff = this.carouselWidth;
    }
    this.renderer.setStyle(this.cardElm.nativeElement, 'right', this.diff + 'px');
    this.renderer.setStyle(this.scrollElm.nativeElement, 'left', ((this.carouselViewPort / (370 * this.totalBookings)) * this.diff) + 'px');
  }

  touchEvent(event) {
    event.preventDefault();
    const coord = event.changedTouches[0].pageX;
    if (event.type === 'touchstart') {
      this.calculateCarouselScrollLimit();
      this.tsCoord = coord;
    } else if (event.type === 'touchmove') {
      this.scrollTheCarousel(coord);
    } else {
      this.rightPos = this.diff;
    }
  }

  mouseEvent(event) {
    event.preventDefault();
    if (event.type === 'mousedown') {
      // this.calculateCarouselScrollLimit();
      this.tsCoord = event.pageX;
      if (!this.isListenerActive) {
        this.mouseMoveListener = this.renderer.listen('document', 'mousemove', (mmevent) => {
          event.preventDefault();
          const coord = mmevent.pageX;
          this.scrollTheCarousel(coord);
        });
        this.mouseUpListener = this.renderer.listen('document', 'mouseup', (muevent) => {
          event.preventDefault();
          this.rightPos = this.diff;
          this.mouseMoveListener();
          this.isListenerActive = false;
          this.mouseUpListener();
        });
        this.isListenerActive = true;
      }
    }
  }

  wheelEvent(event) {
    event.preventDefault();
    // console.log(event);
    const coord = event.wheelDeltaY || event.wheelDeltaX;
    this.tsCoord = event.pageX;
    this.diff = this.rightPos - coord;
    if (this.diff < 0) {
      this.diff = 0;
    }
    if (this.diff > this.carouselWidth) {
      this.diff = this.carouselWidth;
    }
    this.renderer.setStyle(this.cardElm.nativeElement, 'right', this.diff + 'px');
    this.renderer.setStyle(this.scrollElm.nativeElement, 'left', ((this.carouselViewPort / (370 * this.totalBookings)) * this.diff) + 'px');
    this.rightPos = this.diff;

  }

  ngOnDestroy() {
    // this.mouseMoveListener();
    // this.mouseUpListener();
  }

}
