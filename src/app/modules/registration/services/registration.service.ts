import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { RestApiService } from '../../shared/services/rest-api.service';

@Injectable()
export class RegistrationService {

  constructor(private restApiService: RestApiService) { }

  getRegistrationData(data): Observable<any> {
    return this.restApiService.post('user/account-info', data, 'page-center');
  }

  updateRegistrationData(data): Observable<any> {
    return this.restApiService.put('user/user-info', data, 'page-center');
  }
}
