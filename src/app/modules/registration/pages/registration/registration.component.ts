import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import * as moment from 'moment';
import { Message } from 'primeng/components/common/api';

import { AppSettings } from '../../../shared/app.settings';
import { AppConstant } from '../../../shared/app.constant.service';
import { isInvalidCpf } from '../../../shared/services/cpf.validator';
import { isInvalidDob } from '../../../shared/services/dob.validator';
import { RegistrationService } from '../../services/registration.service';
import { StorageService } from '../../../shared/services/storage.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html'
})
export class RegistrationComponent implements OnInit {

  profileImage: any;
  msgs: Message[] = [];
  userId = this.storageService.getItem('userDetails')['UserId'];
  // fileUrl: any = this.storageService.getItem('userImg');
  fileUrl: any = 'assets/images/profile.png';
  registrationForm = new FormGroup({
    cpf: new FormControl(),
    name: new FormControl(),
    email: new FormControl(),
    phone: new FormControl(),
    dob: new FormControl()
  });

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private sanitizer: DomSanitizer,
    private registrationService: RegistrationService,
    private storageService: StorageService
  ) {
    this.getRegistrationData();
  }

  ngOnInit() {
  }

  getRegistrationData() {
    const data = {
      UserId: this.userId,
      UserProfileId: 0
    };
    this.registrationService.getRegistrationData(data).subscribe(results => {
      this.createRegistrationForm(results);
    }, (error) => {
      console.log(error);
    });
  }

  createRegistrationForm(results) {
    const formatedDate = moment(results['DateOfBirth']).format('MM/DD/YYYY').toString();

    this.registrationForm = this.formBuilder.group({
      cpf: [results['CPF'], [Validators.required, isInvalidCpf]],
      name: [results['Name'], [Validators.required]],
      email: [results['Email'], [Validators.required, Validators.pattern(AppConstant.EMAIL_PATTERN)]],
      phone: [results['PhoneNumber'],
      [Validators.required, Validators.pattern(AppConstant.DIGIT_PATTERN), Validators.maxLength(AppConstant.PHONE_MAX_LENGTH)]
      ],
      dob: [formatedDate, [Validators.required, isInvalidDob]]
    });
  }

  onSelect(event) {
    if (event.files.length >= 1 && typeof event.files[0].objectURL !== 'undefined') {
      // debugger;
      this.fileUrl = this.sanitize(event.files[0].objectURL.changingThisBreaksApplicationSecurity);
      // console.log(this.fileUrl['changingThisBreaksApplicationSecurity']);
      this.profileImage = event.files;
    }
  }

  doSubmit() {
    const data = {
      UserId: this.userId,
      CPF: this.registrationForm.controls.cpf.value,
      Name: this.registrationForm.controls.name.value,
      Email: this.registrationForm.controls.email.value,
      PhoneNumber: this.registrationForm.controls.phone.value,
      DateOfBirth: this.registrationForm.controls.dob.value
    };
    this.registrationService.updateRegistrationData(data).subscribe(results => {
      this.storageService.setItem(AppSettings.USER_IMG, this.fileUrl['changingThisBreaksApplicationSecurity']);
      this.msgs = [];
      this.msgs.push({ severity: 'success', summary: 'Success Message', detail: results.Message });
    }, (error) => {
      this.msgs = [];
      this.msgs.push({ severity: 'error', summary: 'Error Message', detail: error.Message });
    });
  }

  sanitize(url: string) {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }
}
