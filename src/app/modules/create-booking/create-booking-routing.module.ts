import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateBookingComponent } from './pages/create-booking/create-booking.component';

const routes: Routes = [
  { path: '', component: CreateBookingComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateBookingRoutingModule { }
