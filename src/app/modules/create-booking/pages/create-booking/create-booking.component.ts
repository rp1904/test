import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SelectItem } from 'primeng/components/common/api';
import { AppSettings } from '../../../shared/app.settings';
import { UserCommonService } from '../../../shared/services/user-common.service';
import { CommonBindingDataService } from '../../../shared/services/common-binding-data.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-create-booking',
  templateUrl: './create-booking.component.html'
})
export class CreateBookingComponent implements OnInit {
  cities: SelectItem[];
  areas: SelectItem[];
  durations: SelectItem[];
  // durationTime: SelectItem[];
  getVehicleChargData: any;
  areaCount = 0;
  selectedCityId;
  isXaximCity: Boolean = false;
  vahicles: any[];
  filteredVehicles: any[];
  message: any[];
  saoPauloCityId = '9999999972';

  createBookingForm: FormGroup = new FormGroup({
    selectedCity: new FormControl(),
    selectedVehicles: new FormControl(),
    selectedDurationType: new FormControl()
  });

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserCommonService,
    private commonBinding: CommonBindingDataService,
    private router: Router) {
  }

  ngOnInit() {
    this.cities = [];
    this.areas = [];
    this.durations = [];
    // this.durationTime = [];
    this.message = [];
    this.areas.push({ label: 'Select City', value: null });
    this.cities.push({ label: 'Select City', value: null });

    // get city list
    this.userService.getCtiyList().subscribe(result => {
      result.forEach(element => {
        this.cities.push({ label: element.CityName, value: { id: element.CityId, name: element.CityName } });
      });
      const index = this.cities.findIndex((datas) => datas.value !== null && datas.value.id === this.saoPauloCityId);
      this.cities.splice(index, 1);
    }, (error) => {
      this.message.push({ severity: 'error', summary: 'Error Message', detail: error.Message });
    });

    // get vehicle list
    this.userService.getVehicleList().subscribe(result => {
      this.vahicles = result;
    }, (error) => {
      this.message.push({ severity: 'error', summary: 'Error Message', detail: error.Message });
    });

    this.createBookingForm = this.formBuilder.group({
      selectedCity: ['', [Validators.required]],
      selectedVehicles: ['', [Validators.required]],
      selectedDurationType: ['', [Validators.required]]
    });

  }

  // select selected city
  getCurrentCity(event: any) {
    this.createBookingForm.controls['selectedDurationType'].reset();
    this.message = [];
    if (event.value === null) {
      // this.durationTime = [];
      this.durations = [];
      this.areas = [];
      this.areaCount = 0;
      this.createBookingForm.removeControl('bookingSlot');
    } else {
      this.selectedCityId = event.value.id;
      // if (this.selectedCityId === '9999999972') { // only for Sao paulo city
      //   this.createBookingForm.removeControl('bookingSlot');
      //   const time = this.getCurrentTimeInSec();
      //   if (time < (11 * 3600)) {
      //     this.createBookingForm.addControl('selectedDurationTime', new FormControl('', [Validators.required]));
      //   }

      //   if (time < (7 * 3600)) {
      //     this.durationTime.push({ label: '07:00', value: '07:00' });
      //     this.durationTime.push({ label: '08:00', value: '08:00' });
      //     this.durationTime.push({ label: '09:00', value: '09:00' });
      //     this.durationTime.push({ label: '10:00', value: '10:00' });
      //   } else if (time < (8 * 3600)) {
      //     this.durationTime.push({ label: 'Active Now', value: '07:00' });
      //     this.durationTime.push({ label: '08:00', value: '08:00' });
      //     this.durationTime.push({ label: '09:00', value: '09:00' });
      //     this.durationTime.push({ label: '10:00', value: '10:00' });
      //   } else if (time < (9 * 3600)) {
      //     this.durationTime.push({ label: 'Active Now', value: '08:00' });
      //     this.durationTime.push({ label: '09:00', value: '09:00' });
      //     this.durationTime.push({ label: '10:00', value: '10:00' });
      //   } else if (time < (10 * 3600)) {
      //     this.durationTime.push({ label: 'Active Now', value: '09:00' });
      //     this.durationTime.push({ label: '10:00', value: '10:00' });
      //   } else if (time < (11 * 3600)) {
      //     this.durationTime.push({ label: 'Active Now', value: '10:00' });
      //   } else {
      //     this.durationTime = [];
      //   }
      // } else if (this.selectedCityId === '9999999968') { // only for Xaxim city

      if (this.selectedCityId === '9999999968') { // only for Xaxim city
        this.createBookingForm.addControl('bookingSlot', new FormControl('',
          [Validators.required, Validators.pattern(AppSettings.DIGIT_PATTERN_CONDITION), Validators.maxLength(4)]
        ));
        this.isXaximCity = true;
      } else {
        // this.durationTime = [];
        // this.createBookingForm.removeControl('selectedDurationTime');
        this.createBookingForm.removeControl('bookingSlot');
        this.isXaximCity = false;
      }

      this.areas = [];
      this.areas.push({ label: 'Select City', value: null });
      if (event.value !== null) {
        const selectedCity = event.value;
        const data = {
          UserId: 1, // load from local storage
          UserProfileId: '0', // load from local storage
          CityId: selectedCity.id
        };
        this.userService.getParkingCharge(data).subscribe(result => {
          this.durations = [];
          this.getVehicleChargData = result;
          this.areaCount = result.CityArea.length;
          if (result.CityArea.length > 0) {
            this.createBookingForm.addControl('selectedArea', new FormControl('', [Validators.required]));
            result.CityArea.forEach(element => {
              this.areas.push({ label: element.NAME, value: { id: element.ID, name: element.NAME } });
            });
          } else {
            this.createBookingForm.removeControl('selectedArea');
            result.CityParkingCharges.forEach(element => {
              this.durations.push({
                label: element.Duration + 'Min' + ' / R$' + (element.Value).toFixed(2),
                value: { id: element.ID, duration: element.Duration }
              });
            });
          }
        }, (error) => {
          this.message.push({ severity: 'error', summary: 'Error Message', detail: error.Message });
        });
      } else {
        this.createBookingForm.removeControl('selectedArea');
      }
    }
  }

  // Create Booking On Submit
  createBooking(event: any) {
    let data;
    // let preData;
    this.message = [];
    if (this.areaCount > 0) {
      // if ((this.selectedCityId === '9999999972')) { // only for Sao Paulo city
      //   if (this.getCurrentTimeInSec() < (11 * 3600)) {
      //     preData = {
      //       UserId: 1, // TBD set login user id
      //       UserProfileId: 0, // TBD set corporate id
      //       DeviceUId: '',
      //       DeviceType: 0,
      //       CityId: this.createBookingForm.get('selectedCity').value.id,
      //       AreaId: this.createBookingForm.get('selectedArea').value.id,
      //       TariffId: this.createBookingForm.get('selectedDurationType').value.id,
      //       VehicleName: this.createBookingForm.get('selectedVehicles').value.VehicleName,
      //       LicensePlate: this.createBookingForm.get('selectedVehicles').value.Plate,
      //       StartTime: this.createBookingForm.get('selectedDurationTime').value,
      //     };
      //     this.userService.getPreBookingData(preData).subscribe(result => {
      //       console.log(result);
      //       this.saoPauloBooking(result);
      //     }, (error) => {
      //       this.message.push({ severity: 'error', summary: 'Error Message', detail: error.Message });
      //     });

      //   } else {
      //     preData = {
      //       UserId: 1, // TBD set login user id
      //       UserProfileId: 0, // TBD set corporate id
      //       DeviceUId: '',
      //       DeviceType: 0,
      //       CityId: this.createBookingForm.get('selectedCity').value.id,
      //       AreaId: this.createBookingForm.get('selectedArea').value.id,
      //       TariffId: this.createBookingForm.get('selectedDurationType').value.id,
      //       VehicleName: this.createBookingForm.get('selectedVehicles').value.VehicleName,
      //       LicensePlate: this.createBookingForm.get('selectedVehicles').value.Plate,
      //       StartTime: '',
      //     };
      //     this.userService.getPreBookingData(preData).subscribe(result => {
      //       console.log(result);
      //       this.saoPauloBooking(result);
      //     }, (error) => {
      //       this.message.push({ severity: 'error', summary: 'Error Message', detail: error.Message });
      //     });
      //   }
      // } else {
      data = {
        UserId: 1, // TBD set login user id
        UserProfileId: 0, // TBD set corporate id
        DeviceUid: 0,
        DeviceType: 0,
        CityId: this.createBookingForm.get('selectedCity').value.id,
        AreaId: this.createBookingForm.get('selectedArea').value.id,
        TariffId: this.createBookingForm.get('selectedDurationType').value.id,
        Vaga: 0,
        VehicleName: this.createBookingForm.get('selectedVehicles').value.VehicleName,
        LicensePlate: this.createBookingForm.get('selectedVehicles').value.Plate,
        BookingType: 'P',
        Latitude: 0,
        Longitude: 0,
        TransactionId: '',
      };
      this.bookParking(data);
      // }
    } else {
      let vagaValue: 0;
      // condition true is only for Xaxim city
      vagaValue = (this.selectedCityId === '9999999968') ? this.createBookingForm.get('bookingSlot').value : 0;
      data = {
        UserId: 1, // TBD set login user id
        UserProfileId: 0, // TBD set corporate id
        DeviceUid: 0,
        DeviceType: 0,
        CityId: this.createBookingForm.get('selectedCity').value.id,
        AreaId: null,
        TariffId: this.createBookingForm.get('selectedDurationType').value.id,
        Vaga: vagaValue,
        VehicleName: this.createBookingForm.get('selectedVehicles').value.VehicleName,
        LicensePlate: this.createBookingForm.get('selectedVehicles').value.Plate,
        BookingType: 'P',
        Latitude: 0,
        Longitude: 0,
        TransactionId: '',
      };
      this.bookParking(data);
    }
  }

  // call create bookig api
  bookParking(data) {
    this.message = [];
    this.userService.createBookParking(data).subscribe(result => {
      this.message.push({ severity: 'success', summary: 'Success Message', detail: this.commonBinding.getLabel('success_parking_msg') });
      this.createBookingForm.reset();
      this.areas = [];
      this.areaCount = 0;
      this.durations = [];
      this.isXaximCity = false;
    }, (error) => {
      this.message.push({ severity: 'error', summary: 'Error Message', detail: error.Message });
    });
  }

  // call sao paulo bookig api
  // saoPauloBooking(preResponse) {
  //   this.message = [];
  //   let extendParkingFlag = 'N';
  //   extendParkingFlag = ((preResponse.ExtendedFlag === null) || (preResponse.ExtendedFlag === 'N')) ? 'N' : 'Y';

  //   const data = {
  //     UserId: 1, // TBD set login user id
  //     ActivateOnTime: preResponse.StartDate,
  //     StartDate: preResponse.StartDate,
  //     EndDate: preResponse.EndDate,
  //     UserProfileId: 0, // TBD set corporate id
  //     DeviceUid: 0,
  //     DeviceType: 0,
  //     CityId: this.createBookingForm.get('selectedCity').value.id,
  //     AreaId: this.createBookingForm.get('selectedArea').value.id,
  //     Tariff: this.createBookingForm.get('selectedDurationType').value.duration,
  //     TariffId: this.createBookingForm.get('selectedDurationType').value.id,
  //     Vaga: 0,
  //     ExtendeFlag: extendParkingFlag,
  //     VehicleName: this.createBookingForm.get('selectedVehicles').value.VehicleName,
  //     LicensePlate: this.createBookingForm.get('selectedVehicles').value.Plate,
  //     BookingType: 'P',
  //     Latitude: 0,
  //     Longitude: 0,
  //     TransactionId: '',
  //     IMEI: '352116066282941' // IMEI number is optionl.
  //   };
  //   this.userService.saoPauloCreateBooking(data).subscribe(result => {
  //     console.log(result);
  //     this.message.push({ severity: 'success', summary: 'Success Message', detail: this.commonBinding.getLabel('success_parking_msg') });
  //     this.createBookingForm.reset();
  //     this.durationTime = [];
  //     this.areas = [];
  //     this.areaCount = 0;
  //     this.durations = [];
  //   }, (error) => {
  //     console.log(error);
  //     this.message.push({ severity: 'error', summary: 'Error Message', detail: error.Message });
  //   });
  // }

  // search for vehicle
  filterVehicles(event: any) {
    this.filteredVehicles = [];
    this.vahicles.forEach(selectedVehicle => {
      const vehicleName = (selectedVehicle.VehicleName === null) ? '' : selectedVehicle.VehicleName;
      if (
        (vehicleName.toLowerCase().indexOf(event.query.toLowerCase()) >= 0) ||
        (selectedVehicle.Plate.toLowerCase().indexOf(event.query.toLowerCase()) >= 0)
      ) {
        this.filteredVehicles.push(selectedVehicle);
      }
    });
  }

  // get Selected Area
  getSelectedArea(event: any) {
    this.durations = [];
    if (event.value !== null) {
      this.getVehicleChargData.CityParkingCharges.forEach(element => {
        if (this.selectedCityId === '9999999972') {
          if (event.value.id === element.AreaID) {
            this.durations.push({
              label: element.Duration + 'Min' + ' / ' + element.Name,
              value: { id: element.ID, duration: element.Duration }
            });
          }
        } else if (event.value.id === element.AreaID) {
          this.durations.push({
            label: element.Duration + 'Min' + ' / R$ ' + (element.Value).toFixed(2),
            value: { id: element.ID, duration: element.Duration }
          });
        }
      });
    } else {
      this.durations = [];
    }
  }

  //   // get current time in seconds
  // getCurrentTimeInSec() {
  //   const date = new Date();
  //   const hoursInSec = date.getHours() * 3600;
  //   const minInSec = date.getMinutes() * 60;
  //   const seconds = date.getSeconds();
  //   const time = hoursInSec + minInSec + seconds;
  //   return time;
  // }
}
