import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SpinnerModule } from 'primeng/primeng';

import { SharedModule } from '../shared/shared.module';
import { CreateBookingRoutingModule } from './create-booking-routing.module';
import { CreateBookingComponent } from './pages/create-booking/create-booking.component';

@NgModule({
  imports: [
    CommonModule,
    CreateBookingRoutingModule,
    SharedModule,
    SpinnerModule
  ],
  declarations: [CreateBookingComponent]
})
export class CreateBookingModule { }
