import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BuyCadsRoutingModule } from './buy-cads-routing.module';
import { BuyCadsComponent } from './pages/buy-cads/buy-cads.component';

@NgModule({
  imports: [
    CommonModule,
    BuyCadsRoutingModule
  ],
  declarations: [BuyCadsComponent]
})
export class BuyCadsModule { }
