import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyCadsComponent } from './buy-cads.component';

describe('BuyCadsComponent', () => {
  let component: BuyCadsComponent;
  let fixture: ComponentFixture<BuyCadsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BuyCadsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyCadsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
