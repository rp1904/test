import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BuyCadsComponent } from './pages/buy-cads/buy-cads.component';

const routes: Routes = [
  { path: '', component: BuyCadsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BuyCadsRoutingModule { }
