import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddSaldoComponent } from './pages/add-saldo/add-saldo.component';
import { SaldoListComponent } from './pages/saldo-list/saldo-list.component';

export const routes: Routes = [
  { path: '', redirectTo: 'list' },
  { path: 'list', component: SaldoListComponent },
  { path: 'add', component: AddSaldoComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BuySaldoRoutingModule { }
