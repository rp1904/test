import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { BuySaldoRoutingModule } from './buy-saldo-routing.module';
import { AddSaldoComponent } from './pages/add-saldo/add-saldo.component';
import { SaldoListComponent } from './pages/saldo-list/saldo-list.component';

@NgModule({
  imports: [
    CommonModule,
    BuySaldoRoutingModule,
    SharedModule
  ],
  declarations: [AddSaldoComponent, SaldoListComponent]
})
export class BuySaldoModule { }
