import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaldoListComponent } from './saldo-list.component';

describe('SaldoListComponent', () => {
  let component: SaldoListComponent;
  let fixture: ComponentFixture<SaldoListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SaldoListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaldoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
