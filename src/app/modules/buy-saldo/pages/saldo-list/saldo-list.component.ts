import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-saldo-list',
  templateUrl: './saldo-list.component.html',
})
export class SaldoListComponent implements OnInit {

  buySaldo = new FormGroup({
    amount: new FormControl(),
  });

  constructor(
    private formBuilder: FormBuilder,
    private router: Router
  ) { }

  ngOnInit() {
    this.buySaldo = this.formBuilder.group({
      amount: ['', [Validators.required]],
    });
  }

  doAddAmount() {
    this.router.navigate(['buy-saldo/add'], {});
  }

}
