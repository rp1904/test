import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SelectItem } from 'primeng/primeng';

@Component({
  selector: 'app-add-saldo',
  templateUrl: './add-saldo.component.html'
})
export class AddSaldoComponent implements OnInit {

  expirymm: SelectItem[] = [];
  expiryyy: SelectItem[] = [];
  showSuccessPopup = false;
  showErrorPopup = false;

  buySaldo = new FormGroup({
    savedCard: new FormControl(''),
    ccno: new FormControl(),
    expirymm: new FormControl(),
    expiryyy: new FormControl(),
    ccv: new FormControl(),
    saveCard: new FormControl()
  });

  constructor(
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this.expirymm.push({ label: '1', value: { id: 1, name: '1', code: '1' } });
    this.expirymm.push({ label: '2', value: { id: 2, name: '2', code: '2' } });
    this.expiryyy.push({ label: '1', value: { id: 1, name: '1', code: '1' } });
    this.expiryyy.push({ label: '2', value: { id: 2, name: '2', code: '2' } });
  }

  ngOnInit() {
    this.buySaldo = this.formBuilder.group({
      savedCard: ['', [Validators.required]],
      ccno: ['', [Validators.required]],
      expirymm: ['', [Validators.required]],
      expiryyy: ['', [Validators.required]],
      ccv: ['', [Validators.required]],
      saveCard: ['']
    });
  }

  doProceed() {
    this.showSuccessPopup = true;
  }

  doCancel() {
  }

  successUpdateClick() {
    this.router.navigate(['buy-saldo/list'], {});
  }

  errorUpdateClick() {
    this.showErrorPopup = false;
  }
}
