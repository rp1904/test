import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSaldoComponent } from './add-saldo.component';

describe('AddSaldoComponent', () => {
  let component: AddSaldoComponent;
  let fixture: ComponentFixture<AddSaldoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AddSaldoComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSaldoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
