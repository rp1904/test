import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BonusRedemptionComponent } from './pages/bonus-redemption/bonus-redemption.component';
import { BonusListComponent } from './pages/bonus-list/bonus-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'list' },
  { path: 'list', component: BonusListComponent, pathMatch: 'full' },
  { path: 'add-bonus', component: BonusRedemptionComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BonusRedemptionRoutingModule { }
