import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BonusRedemptionComponent } from './bonus-redemption.component';

describe('BonusRedemptionComponent', () => {
  let component: BonusRedemptionComponent;
  let fixture: ComponentFixture<BonusRedemptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BonusRedemptionComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BonusRedemptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
