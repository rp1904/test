import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserCommonService } from '../../../shared/services/user-common.service';
import { SelectItem } from 'primeng/primeng';
import { CommonBindingDataService } from '../../../shared/services/common-binding-data.service';
import { vehiclePlateValidator } from '../../../shared/services/vehicle-plate.validator';

@Component({
  selector: 'app-bonus-redemption',
  templateUrl: './bonus-redemption.component.html'
})
export class BonusRedemptionComponent implements OnInit {

  cities: SelectItem[];
  message: any[];

  bonusRedemptionForm = new FormGroup({
    selectedCity: new FormControl(),
    warningNumber: new FormControl(),
    numberPlate: new FormControl()
  });

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserCommonService,
    private commonBinding: CommonBindingDataService
  ) { }

  ngOnInit() {
    this.message = [];
    this.cities = [];
    this.cities.push({ label: 'Select City', value: null });
    this.userService.getCtiyList().subscribe(result => {
      result.forEach(element => {
        this.cities.push({ label: element.CityName, value: { id: element.CityId, name: element.CityName } });
      });
    }, (error) => {
      this.message.push({ severity: 'error', summary: 'Error Message', detail: error.Message });
    });

    this.bonusRedemptionForm = this.formBuilder.group({
      selectedCity: ['', [Validators.required]],
      warningNumber: ['', [Validators.required, Validators.maxLength(20)]],
      numberPlate: ['', [Validators.required, vehiclePlateValidator]]
    });
  }

  bonusFormAction(value: any) {
    this.message = [];
    const data = {
      UserId: 0,
      ProfileID: 0,
      CityId: this.bonusRedemptionForm.get('selectedCity').value.id,
      ACT: this.bonusRedemptionForm.get('warningNumber').value.trim(),
      LicensePlate: this.bonusRedemptionForm.get('numberPlate').value.trim().replace(/ /g, '').toUpperCase()
    };
    this.userService.addBonusRedemption(data).subscribe(result => {
      this.message.push({ severity: 'success', summary: 'Success Message', detail: this.commonBinding.getLabel('success_bonus_added') });
    }, (error) => {
      this.message.push({ severity: 'error', summary: 'Error Message', detail: error.Message });
    });
  }

  navigateToBonusList(event: any) {
    this.router.navigate(['bonus-redemption/list'], {});
  }

}
