import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReactiveFormsModule } from '@angular/forms';

import { DropdownModule, ButtonModule } from 'primeng/primeng';
import { SharedModule } from '../../modules/shared/shared.module';

import { BonusRedemptionRoutingModule } from './bonus-redemption-routing.module';
import { BonusRedemptionComponent } from './pages/bonus-redemption/bonus-redemption.component';
import { BonusListComponent } from './pages/bonus-list/bonus-list.component';

@NgModule({
  imports: [
    CommonModule,
    BonusRedemptionRoutingModule,
    DropdownModule,
    ButtonModule,
    SharedModule,
    ReactiveFormsModule
  ],
  declarations: [BonusRedemptionComponent, BonusListComponent]
})
export class BonusRedemptionModule { }
