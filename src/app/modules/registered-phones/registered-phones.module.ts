import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegisteredPhonesRoutingModule } from './registered-phones-routing.module';
import { RegisteredPhonesListComponent } from './pages/registered-phones-list/registered-phones-list.component';

@NgModule({
  imports: [
    CommonModule,
    RegisteredPhonesRoutingModule
  ],
  declarations: [RegisteredPhonesListComponent]
})
export class RegisteredPhonesModule { }
