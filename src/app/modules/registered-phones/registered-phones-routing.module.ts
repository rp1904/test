import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisteredPhonesListComponent } from './pages/registered-phones-list/registered-phones-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'list' },
  { path: 'list', component: RegisteredPhonesListComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegisteredPhonesRoutingModule { }
