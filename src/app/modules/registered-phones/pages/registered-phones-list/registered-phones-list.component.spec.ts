import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisteredPhonesListComponent } from './registered-phones-list.component';

describe('RegisteredPhonesListComponent', () => {
  let component: RegisteredPhonesListComponent;
  let fixture: ComponentFixture<RegisteredPhonesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RegisteredPhonesListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisteredPhonesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
