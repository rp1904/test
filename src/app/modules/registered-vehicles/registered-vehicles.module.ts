import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { RegisteredVehiclesRoutingModule } from './registered-vehicles-routing.module';
import { RegisteredVehiclesListComponent } from './pages/registered-vehicles-list/registered-vehicles-list.component';
import { EditableRegisteredVehiclesComponent } from './pages/editable-registered-vehicles/editable-registered-vehicles.component';
import { VehicleService } from './services/vehicle.service';

@NgModule({
  imports: [
    CommonModule,
    RegisteredVehiclesRoutingModule,
    SharedModule
  ],
  declarations: [RegisteredVehiclesListComponent, EditableRegisteredVehiclesComponent],
  providers: [VehicleService]
})

export class RegisteredVehiclesModule { }
