import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Response } from '@angular/http';
import 'rxjs/add/operator/map';

import { RestApiService } from '../../shared/services/rest-api.service';
import { VehicleModel } from '../models/vehicle.model';

@Injectable()
export class VehicleService {

  vehicles: VehicleModel[] = new Array<VehicleModel>();
  constructor(private restApiService: RestApiService) {

  }

  getVehicles(): Observable<any> {
    return this.restApiService.get('vehicle', 'page-center');
  }
  addVehicles(vehicleDto: VehicleModel): Observable<any> {
    return this.restApiService.post('vehicle', vehicleDto, 'page-center');
  }

  updateVehicle(vehicleDto: VehicleModel): Observable<any> {
    return this.restApiService.put('vehicle', vehicleDto, 'page-center');
  }

  deleteVehicle(vehicleId: Number): Observable<any> {
    return this.restApiService.delete(`vehicle/${vehicleId}`, 'page-center');
  }
}
