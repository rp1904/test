import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisteredVehiclesListComponent } from './registered-vehicles-list.component';

describe('RegisteredVehiclesListComponent', () => {
  let component: RegisteredVehiclesListComponent;
  let fixture: ComponentFixture<RegisteredVehiclesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RegisteredVehiclesListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisteredVehiclesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
