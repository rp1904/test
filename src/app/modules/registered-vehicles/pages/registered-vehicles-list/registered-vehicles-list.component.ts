import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { ConfirmationService } from 'primeng/components/common/api';

import { VehicleModel } from '../../models/vehicle.model';
import { UiService } from '../../../shared/services/ui.service';
import { CommonBindingDataService } from '../../../shared/services/common-binding-data.service';
import { VehicleService } from '../../services/vehicle.service';
import { AppConstant } from '../../../shared/app.constant.service';

@Component({
  selector: 'app-registered-vehicles-list',
  templateUrl: './registered-vehicles-list.component.html'
})
export class RegisteredVehiclesListComponent implements OnInit {

  public static subjectEvent: Subject<any> = new Subject();
  isDetailView: Boolean = false;
  messages: any[];

  constructor(private router: Router, private uiService: UiService,
    private vehicleService: VehicleService,
    private confirmationService: ConfirmationService,
    private common: CommonBindingDataService,
    public appConstant: AppConstant) {
  }

  ngOnInit() {
    this.vehicleService.getVehicles().subscribe(
      (result) => {
        this.vehicleService.vehicles = result;
        // Fire event for child when load list.
        RegisteredVehiclesListComponent.subjectEvent.next(true);
      }, (error) => {
      });
  }

  onAddVehicleClick() {
    this.router.navigate(['registered-vehicles/add', 0]);
  }

  editVehicle(vehicle: VehicleModel) {
    this.router.navigate(['registered-vehicles/edit', vehicle.VehicleId]);
  }

  deleteVehicle(vehicleId: Number) {
    this.messages = [];
    this.vehicleService.deleteVehicle(vehicleId)
      .subscribe((result) => {
        const deletedVehicleIndex = this.vehicleService.vehicles.findIndex((vehicle) => vehicle.VehicleId === vehicleId);
        if (deletedVehicleIndex !== null) {
          this.vehicleService.vehicles.splice(deletedVehicleIndex, 1);
          this.messages = [{ severity: 'success', summary: 'Success message', detail: this.common.getLabel('msg_vehicle_delete_success') }];
        }
      }, (error) => {

      });
  }

  // Event for remove child component from view
  componentRemoved(event) {
    this.isDetailView = false;
  }

  // Event for load  child component
  componentAdded(event) {
    this.isDetailView = true;
  }

  onShowDeletePopup(vehicleId: Number) {
    this.confirmationService.confirm({
      message: this.common.getLabel('lbl_delete_vehicle_confirm'),
      header: this.common.getLabel('lbl_delete_confirmation'),
      icon: 'fa fa-trash',
      accept: () => {
        this.deleteVehicle(vehicleId);
      },
      reject: () => {

      }
    });
  }
}
