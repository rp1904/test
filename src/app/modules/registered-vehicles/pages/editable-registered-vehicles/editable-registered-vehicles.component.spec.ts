import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditableRegisteredVehiclesComponent } from './editable-registered-vehicles.component';

describe('EditableRegisteredVehiclesComponent', () => {
  let component: EditableRegisteredVehiclesComponent;
  let fixture: ComponentFixture<EditableRegisteredVehiclesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EditableRegisteredVehiclesComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditableRegisteredVehiclesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
