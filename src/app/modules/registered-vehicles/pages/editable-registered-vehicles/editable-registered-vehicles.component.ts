import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { SelectItem } from 'primeng/primeng';

import { VehicleModel } from '../../models/vehicle.model';
import { VehicleService } from '../../services/vehicle.service';
import { RegisteredVehiclesListComponent } from '../registered-vehicles-list/registered-vehicles-list.component';
import { CommonBindingDataService } from '../../../shared/services/common-binding-data.service';
import { AppConstant } from '../../../shared/app.constant.service';
import { vehiclePlateValidator } from '../../../shared/services/vehicle-plate.validator';
import { DigiPareCommonService } from '../../../shared/services/digipare-common.service';

@Component({
  selector: 'app-editable-registered-vehicles',
  templateUrl: './editable-registered-vehicles.component.html'
})
export class EditableRegisteredVehiclesComponent implements OnInit {
  vehicles: SelectItem[];

  editableVehicle: FormGroup;
  selectedVehicle: VehicleModel;
  message: any[];
  isEditVehicle: Boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private activateRoute: ActivatedRoute,
    private vehicleService: VehicleService,
    private common: CommonBindingDataService,
    private appConstant: AppConstant,
    private commonService: DigiPareCommonService
  ) {
    RegisteredVehiclesListComponent.subjectEvent.subscribe(() => {
      this.initSetup();
    });
  }

  ngOnInit() {
    this.vehicles = [];
    this.vehicles.push({ label: 'Car', value: this.appConstant.VEHICLE_TYPE.CAR });
    this.vehicles.push({ label: 'Motorcycle', value: this.appConstant.VEHICLE_TYPE.MOTORCYCLE });
    this.vehicles.push({ label: 'Truck', value: this.appConstant.VEHICLE_TYPE.TRUCK });
    this.vehicles.push({ label: 'Bus', value: this.appConstant.VEHICLE_TYPE.BUS });

    this.initSetup();
  }

  initSetup() {
    // Get vehicle id.
    let vehicleId = this.activateRoute.snapshot.params['id'];
    vehicleId = (vehicleId !== null && vehicleId !== '') ? parseInt(vehicleId, 10) : 0;
    this.selectedVehicle = new VehicleModel();
    this.editableVehicle = this.createVehicleForm(this.selectedVehicle);

    this.isEditVehicle = false;

    if (vehicleId > 0) {
      this.isEditVehicle = true;
      this.selectedVehicle = this.vehicleService.vehicles.find((vehicle) => vehicle.VehicleId === vehicleId);
      if (this.selectedVehicle !== null && this.selectedVehicle !== undefined) {
        this.editableVehicle = this.createVehicleForm(this.selectedVehicle);
      } else {
        this.selectedVehicle = new VehicleModel();
      }
    }
  }

  onGoToPrevPageClick() {
    this.router.navigate(['registered-vehicles']);
  }

  saveVehicle() {
    this.message = [];
    const vehicleModel = this.mapVehicleFormToDto();
    vehicleModel.Plate = this.commonService.removeSpaces(vehicleModel.Plate);
    // Update existing vehicle
    if (vehicleModel.VehicleId > 0) {

      this.vehicleService.updateVehicle(vehicleModel).subscribe(
        (data) => {
          if (data !== null) {
            // Update vehicle list.
            this.selectedVehicle = this.vehicleService.vehicles
              .find((vehicle) => vehicle.VehicleId === vehicleModel.VehicleId);

            this.selectedVehicle.VehicleName = data.VehicleName;
            this.selectedVehicle.IsAutoDebit = data.IsAutoDebit;
            this.message.push({
              severity: 'success', summary: 'Success Message',
              detail: this.common.getLabel('msg_vehicle_update_success')
            });

            this.onGoToPrevPageClick();
          }

        }, (error) => {

        });
    } else { // Add new vehicle
      this.vehicleService.addVehicles(vehicleModel).subscribe(
        (data) => {
          if (data !== null) {
            this.vehicleService.vehicles.push(data);
            this.message.push({ severity: 'success', summary: 'Success Message', detail: 'Vehicle added successfully.' });
            this.onGoToPrevPageClick();
          }
        }, (error) => {

        });
    }
  }


  private createVehicleForm(vehicleModel: VehicleModel): FormGroup {
    return this.formBuilder.group({
      VehicleName: [vehicleModel.VehicleName, Validators.required],
      Plate: new FormControl({ value: vehicleModel.Plate, disabled: this.isEditVehicle }, [Validators.required, vehiclePlateValidator]),
      VehicleType: [vehicleModel.VehicleType, [Validators.required]],
      IsAutoDebit: [vehicleModel.IsAutoDebit]
    });
  }

  private mapVehicleFormToDto() {
    const vehicleModel = new VehicleModel();
    const formControls = this.editableVehicle.controls;
    vehicleModel.VehicleId = this.selectedVehicle.VehicleId;
    vehicleModel.VehicleName = formControls['VehicleName'].value;
    vehicleModel.Plate = formControls['Plate'].value;
    vehicleModel.VehicleType = formControls['VehicleType'].value;
    vehicleModel.IsAutoDebit = formControls['IsAutoDebit'].value;

    return vehicleModel;
  }

}
