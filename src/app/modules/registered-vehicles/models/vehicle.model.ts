export class VehicleModel {
  VehicleId: Number = 0;
  Plate: String = '';
  PhoneNumber: Number = 0;
  Phone: String = '';
  VehicleType: Number = 0;
  IsActive: String = '';
  UserId: Number = 0;
  Image: String = '';
  CarMotorcycle: String = '';
  IsAutoDebit: Boolean = false;
  VehicleName: String = '';
  UserProfileId: Number = 0;
}
