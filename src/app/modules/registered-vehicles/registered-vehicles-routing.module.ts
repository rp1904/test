import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisteredVehiclesListComponent } from './pages/registered-vehicles-list/registered-vehicles-list.component';
import { EditableRegisteredVehiclesComponent } from './pages/editable-registered-vehicles/editable-registered-vehicles.component';

const routes: Routes = [
  {
    path: '', component: RegisteredVehiclesListComponent,
    children: [
      {
        path: 'add/:id',
        component: EditableRegisteredVehiclesComponent
      },
      {
        path: 'edit/:id',
        component: EditableRegisteredVehiclesComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegisteredVehiclesRoutingModule { }
