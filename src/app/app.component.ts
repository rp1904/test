import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  // TODO Take this from localization
  title = 'Mobisoft Cab Booking Management Solution';
  showSidebar = true;
  rightColClass = 'col-md-9';

  constructor() {

  }

  ngOnInit() {
    if (!this.showSidebar) {
      this.rightColClass = 'col-md-12';
    }
  }
}
